#cloud-config
write_files:
  - path: "/etc/hstack/hstack.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      PRIVATE_NETWORK="${ private_network }"
      CONSUL_MODE="agent"
      CONSUL_AUTOJOIN="aws"
      CONSUL_AUTOJOIN_AWS_TAG_VALUE="${ autojoin_tag_value }"
      CONSUL_AUTOJOIN_AWS_TAG_KEY="${ autojoin_tag_key }"
      CONSUL_AGENT_TAGS="nomad-server,${ autojoin_tag_value }"
      CONSUL_ENCRYPT_KEY=${ consul_encrypt_key }
      DOMAIN=${ domain }
      DATACENTER=${ dc }
      JOIN_IPV4_ADDR="${ join_ipv4_addr }"
      NOMAD_MODE=${ mode }
      NOMAD_BOOTSTRAP_EXPECT=${ server_count }
      NOMAD_ENCRYPT_KEY=${ nomad_encrypt_key }
  - path: "/etc/ssl/certs/rootca.pem"
    permissions: "0644"
    owner: "root"
    encoding: base64
    content: ${root_ca}
