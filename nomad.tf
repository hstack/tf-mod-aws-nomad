#--------------------------------------------------------------
# This module creates all resources necessary for Nomad
#--------------------------------------------------------------

data "atlas_artifact" "nomad_ami" {
  name = "${var.atlas_username}/aws-${var.region}-coreos-hstack"

  type = "amazon.image"
  metadata {
     version = "${var.hstack_version}"
  }
}

data "aws_subnet" "selected" {
  id = "${element(split(",",var.subnet_ids), 0)}"
}

data "aws_vpc" "selected" {
  id = "${data.aws_subnet.selected.vpc_id}"
}

data "template_file" "nomad_user_data" {
  template = "${file("${path.module}/nomad.tpl")}"

  vars {
    join_ipv4_addr        = "${var.join_ipv4_addr}"
    private_network       = "${data.aws_vpc.selected.cidr_block}"
    autojoin_tag_key      = "${var.autojoin_tag_key}"
    autojoin_tag_value    = "${coalesce(var.autojoin_tag_value, var.name)}"
    domain                = "${var.domain}"
    dc                    = "${var.dc}"
    root_ca               = "${base64encode(var.root_ca)}"
    nomad_encrypt_key     = "${var.nomad_encrypt_key}"
    consul_encrypt_key    = "${var.consul_encrypt_key}"
    mode                  = "${var.server_mode ? "server": "client"}"
    server_count          = "${var.server_mode ? var.count: ""}"
    tags                  = "${var.tags}"
  }
}

resource "aws_launch_configuration" "nomad" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix                 = "${var.name}"
  iam_instance_profile        = "${var.iam_instance_profile}"
  image_id                    = "${lookup(data.atlas_artifact.nomad_ami.metadata_full, format("region-%s", var.region))}"
  instance_type               = "${var.instance_type}"
  security_groups             = ["${split(",",var.security_group_ids)}"]

  associate_public_ip_address = "false"
  key_name                    = "${var.keypair_name}"

  user_data                   = "${data.template_file.nomad_user_data.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

//  Auto-scaling group for our cluster.
resource "aws_autoscaling_group" "nomad" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix = "${var.name}"
  launch_configuration = "${aws_launch_configuration.nomad.name}"
  desired_capacity     = "${var.count}"
  min_size             = "${var.count}"
  max_size             = "${var.count}"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0

  vpc_zone_identifier = [ "${split(",",var.subnet_ids)}"]

  enabled_metrics           = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "${var.autojoin_tag_key}"
    value               = "${coalesce(var.autojoin_tag_value, var.name)}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
