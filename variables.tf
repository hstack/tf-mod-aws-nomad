
variable "name"                 { default = "nomad" }
variable "hstack_version"       { default = "v1.0"}
variable "atlas_username"       { default = "hstack" }
variable "region"               { default = "us-east-1" }

variable "domain"               { default = "consul" }
variable "dc"                   { default = "dc1" }

variable "subnet_ids"           { }
variable "keypair_name"         { }
variable "security_group_ids"   { }
variable "consul_encrypt_key"   { }

# nomad encrypt key is not mandatory for client nodes
variable "nomad_encrypt_key"    { default = "" }

variable "count"                { default = "-1" }
variable "server_mode"          { default = false }
variable "instance_type"        { default = "t2.medium" }

variable "iam_instance_profile"  { default = "consul_node" }

variable "join_ipv4_addr"       { default = "" }
variable "autojoin_tag_value"   { default = "" }
variable "autojoin_tag_key"     { default = "consul_autojoin" }
variable "tags"                 { default = "" }
variable "root_ca"              { default = "" }
